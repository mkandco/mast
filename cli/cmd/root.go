package cmd

import (
	"github.com/spf13/cobra"
	// log "github.com/sirupsen/logrus"
	// "github.com/spf13/cobra/doc"
)

var Verbose bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "mast",
	Short:   "Mast is a tool for creating and running ansible jobs inside kubernetes cluster",
	Long:    `Mast is a tool for simplifying jobs and tasks performed on kubernetes. This CLI allows to manage all mast resources such as jobs, runs and operators to perform various actions, from provisioning object in cluster to running as automation system`,
	Version: "0.3.0",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())

	//generate docs in ./docs folder when go run main.go
	// err := doc.GenMarkdownTree(rootCmd, "./docs")
	// if err != nil {
	// 	log.Fatal(err)
	// }
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
