package cmd

import (
	"context"
	"os"
	"time"

	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func init() {
	runCmd.AddCommand(runInfoCmd)
	log.SetLevel(log.InfoLevel)
}

var runInfoCmd = &cobra.Command{
	Use:   "info",
	Short: "get info about mastrun",
	Long:  `get datailed info about mastrun by providing its name`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}

		var status string
		var startedTime time.Time
		var finishedTime time.Time
		var duration time.Duration
		var gatherDependenciesMessage string
		dateFormat := "2006-01-02T15:04:05Z"

		dynamicClient := connectToK8s()

		mastrun, err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Get(context.TODO(), args[0], metav1.GetOptions{})
		if err != nil {
			log.Fatal(err)
		}
		startedAt, isStartedAtFound, err := unstructured.NestedString(mastrun.Object, "metadata", "creationTimestamp")
		if isStartedAtFound == true {
			startedTime, err = time.Parse(dateFormat, startedAt)
		} else if err != nil {
			log.Fatal(err)
		}
		mastjob, isMastJobFound, err := unstructured.NestedString(mastrun.Object, "spec", "mastJobRef", "name")
		if err != nil || isMastJobFound == false {
			log.Fatal(err)
		}

		workflowName, isWorkFlow, err := unstructured.NestedString(mastrun.Object, "status", "mastrun_handler", "workFlow")
		if err != nil {
			log.Fatal(err)
		} else if workflowName == "None" {
			handlerMessage, isHandlerMessage, err := unstructured.NestedString(mastrun.Object, "status", "mastrun_handler", "message")
			if err != nil || isHandlerMessage == false {
				log.Fatal(err)
			}
			printInfo(mastrun.GetName(), mastjob, "Completed", "Failed", startedTime.String(), "", "", handlerMessage, "")
			os.Exit(0)
		} else if isWorkFlow == false {
			printInfo(mastrun.GetName(), mastjob, status, string("template"), startedTime.String(), finishedTime.String(), duration.String(), gatherDependenciesMessage, "")
			os.Exit(0)
		} else {
			workflow, err := dynamicClient.Resource(workflowGVR).Namespace("mast").Get(context.TODO(), workflowName, metav1.GetOptions{})
			if err != nil {
				log.Fatal(err)
			}

			isCompleted, isCompletedFound, err := unstructured.NestedString(workflow.Object, "metadata", "labels", "workflows.argoproj.io/completed")
			if isCompleted == "false" || isCompletedFound == false {
				status = "Running"
			} else if isCompleted == "true" {
				status = "Completed"
			} else if err != nil {
				log.Fatal(err)
			}

			result, isResultFound, err := unstructured.NestedString(workflow.Object, "metadata", "labels", "workflows.argoproj.io/phase")
			if err != nil {
				log.Fatal(err)
			} else if isResultFound == false {
				result = ""
			}

			finishedAt, isfinishedAtFound, err := unstructured.NestedFieldCopy(workflow.Object, "status", "finishedAt")
			if err != nil {
				log.Fatal(err)
			} else if isfinishedAtFound == true && finishedAt != nil {
				finishedTime, err = time.Parse(dateFormat, finishedAt.(string))
				duration = finishedTime.Sub(startedTime)
			} else {
				duration = time.Now().Sub(startedTime)
			}

			if result == "Failed" {
				errorStep, isStepFound, err := unstructured.NestedStringSlice(workflow.Object, "status", "nodes", workflow.GetName(), "outboundNodes")
				if isStepFound == false {
					log.Fatal()
				} else if err != nil {
					log.Fatal(err)
				}

				errorStepName, isErrorStepName, err := unstructured.NestedString(workflow.Object, "status", "nodes", errorStep[0], "displayName")
				if err != nil {
					log.Fatal("Something went wrong, try again")
				} else if isErrorStepName == false {

				}
				message, isMessage, err := unstructured.NestedString(workflow.Object, "status", "nodes", errorStep[0], "message")
				if isMessage == false {
					log.Fatal("fetching error message failed")
				}
				if errorStepName == "gather-artifacts" {
					printInfo(mastrun.GetName(), mastjob, status, result, startedTime.String(), finishedTime.String(), duration.String(), message, "")
					os.Exit(0)
				} else {
					printInfo(mastrun.GetName(), mastjob, status, result, startedTime.String(), finishedTime.String(), duration.String(), "ok", message)
					os.Exit(0)
				}
			} else {
				printInfo(mastrun.GetName(), mastjob, status, result, startedTime.String(), finishedTime.String(), duration.String(), gatherDependenciesMessage, "")
				os.Exit(0)
			}
		}

	}}

func printInfo(name string,
	mastjob string,
	status string,
	result string,
	startedTime string,
	finishedTime string,
	duration string,
	gatherDependenciesMessage string,
	playbookMessage string) {

	table := tablewriter.NewWriter(os.Stdout)
	setTableConfigs(table)
	table.Append([]string{"Name:", name})
	table.Append([]string{"MastJob:", mastjob})
	table.Append([]string{"Status:", status})
	table.Append([]string{"Result:", result})
	table.Append([]string{"Started:", startedTime})
	table.Append([]string{"Finished:", finishedTime})
	table.Append([]string{"Duration:", duration})
	table.Append([]string{"", ""})
	table.Append([]string{"Stages:", "Status"})
	table.Append([]string{"Gather dependencies", gatherDependenciesMessage})
	table.Append([]string{"Ansible", playbookMessage})

	table.Render()
}
