package cmd

import (
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/olekukonko/tablewriter"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/tools/clientcmd"
)

func connectToK8s() dynamic.Interface {
	//get kubeconfig
	kubeconfig := filepath.Join(
		os.Getenv("HOME"), ".kube", "config")
	log.Debug("Found kubeconfig file: " + kubeconfig)
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		log.Fatal(err)
	}
	log.Debug("Got config for client from " + kubeconfig)
	// create k8s client
	dynamicClient, err := dynamic.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	log.Debug("Client connected")

	return dynamicClient
}

func getResourceAge(resource unstructured.Unstructured) string {
	timestamp, isTimestamp, err := unstructured.NestedString(resource.Object, "metadata", "creationTimestamp")
	if err != nil {
		log.Fatal(err)
	} else if isTimestamp == false {
		log.Fatal("Error there is no creationTimestamp in resource")
	}

	layout := "2006-01-02T15:04:05Z"
	createdAt, err := time.Parse(layout, timestamp)
	age := time.Now().UTC().Sub(createdAt)
	if err != nil {
		log.Fatal(err)
	}
	if age.Hours() > 7*24 {
		return strconv.FormatFloat(age.Hours()/(7*24), 'f', 0, 64) + "w"
	} else if age.Hours() > 48 {
		return strconv.FormatFloat(age.Hours()/24, 'f', 0, 64) + "d"
	} else if age.Hours() > 1 {
		return strconv.FormatFloat(age.Round(time.Hour).Hours(), 'f', 0, 64) + "h"
	} else if age.Minutes() > 1 {
		return strconv.FormatFloat(age.Round(time.Minute).Minutes(), 'f', 0, 64) + "m"
	}
	return strconv.FormatFloat(age.Round(time.Second).Seconds(), 'f', 0, 64) + "s"
}

func getObjectDef(objectPath string) []byte {
	//check if objectPath is path or url
	if strings.Contains(objectPath, "http") || strings.Contains(objectPath, "https") {
		response, err := http.Get(objectPath)
		if err != nil {
			log.Fatal(err)
		}

		defer response.Body.Close()

		if response.StatusCode != 200 {
			log.Fatal(response.Status)
		}
		log.Debug("File fetched:" + objectPath)
		def, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		return def
	} else {
		if !(strings.Contains(objectPath, ".yaml") || strings.Contains(objectPath, ".yml")) {
			log.Fatal("File must be .yaml or .yml")
		}
		//check if file exists
		if _, err := os.Stat(objectPath); os.IsNotExist(err) {
			log.Fatal(err)
		}
		log.Debug("File exists: " + objectPath)
		//read file
		def, err := ioutil.ReadFile(objectPath)
		if err != nil {
			log.Fatal(err)
		}
		log.Debug("Reading file: " + objectPath)
		return def
	}

}

func convertFileToMap(file []byte) map[string]interface{} {
	resource := make(map[string]interface{})
	err := yaml.Unmarshal(file, &resource)
	if err != nil {
		log.Fatal()
	}
	log.Debug("Converted file to map object")

	return resource
}

func setTableConfigs(table *tablewriter.Table) {
	table.SetAutoWrapText(false)
	table.SetAutoFormatHeaders(true)
	table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetCenterSeparator("")
	table.SetColumnSeparator("")
	table.SetRowSeparator("")
	table.SetHeaderLine(false)
	table.SetBorder(false)
	table.SetTablePadding("\t") // pad with tabs
	table.SetNoWhiteSpace(true)
}

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func RandString(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}
