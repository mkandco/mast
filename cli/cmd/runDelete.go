package cmd

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func init() {
	runCmd.AddCommand(runDeleteCmd)
	log.SetLevel(log.InfoLevel)
}

var runDeleteCmd = &cobra.Command{
	Use:   "delete [run name or file]",
	Short: "Delete mastrun",
	Long:  `Delete mastrun providing the name of the job or file`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if Verbose {
			log.SetLevel(log.DebugLevel)
		}
		dynamicClient := connectToK8s()

		deletePolicy := metav1.DeletePropagationForeground
		deleteOptions := metav1.DeleteOptions{
			PropagationPolicy: &deletePolicy,
		}

		// if arg is a file
		if strings.Contains(args[0], ".yaml") || strings.Contains(args[0], ".yml") {
			file := getObjectDef(args[0])

			//convert yaml to map
			run := convertFileToMap(file)

			// get resource name
			var name = run["metadata"].(map[string]interface{})["name"].(string)
			log.Debug("Found run with name: " + name)
			//delete object
			if err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Delete(context.TODO(), name, deleteOptions); err != nil {
				log.Fatal(err)
			}
			log.Info(name, " run deleted")

		} else {
			if err := dynamicClient.Resource(mastRunGVR).Namespace("mast").Delete(context.TODO(), args[0], deleteOptions); err != nil {
				log.Fatal(err)
			}
			log.Info(args[0], " run deleted")
		}

	},
}
