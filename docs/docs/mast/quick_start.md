# Getting Started

If you would like to give mast a fast try, here is how to install it and run a famous "Hello World".

To do this, you need to Kubernetes cluster and kubectl CLI, in order to set up and access the cluster.

- [kind](https://kind.sigs.k8s.io/)
- [minikube](https://minikube.sigs.k8s.io/docs/)
- Docker Desktop
- k3s
- k3d or any full-fledged cluster

### Install Mast server

Head over to the [releases page](https://gitlab.com/ansi-services/mast/-/releases) and install server and cli. The latest release labeled _Stable_ is recommended.

The command to install server looks something like that; just remember to replace **version** with your desired version number

```bash
kubectl apply -f https://gitlab.com/api/v4/projects/21527026/packages/generic/mast/<VERSION>/mast-installer.yaml
```

??? example "output"
    ```bash 
    namespace/mast created 
    customresourcedefinition.apiextensions.k8s.io/clusterworkflowtemplates.argoproj.io created 
    customresourcedefinition.apiextensions.k8s.io/cronworkflows.argoproj.io created 
    customresourcedefinition.apiextensions.k8s.io/mastjobs.mast.ansi.services created 
    customresourcedefinition.apiextensions.k8s.io/mastoperators.mast.ansi.services created 
    customresourcedefinition.apiextensions.k8s.io/mastruns.mast.ansi.services created 
    customresourcedefinition.apiextensions.k8s.io/workfloweventbindings.argoproj.io created 
    customresourcedefinition.apiextensions.k8s.io/workflows.argoproj.io created 
    customresourcedefinition.apiextensions.k8s.io/workflowtasksets.argoproj.io created 
    customresourcedefinition.apiextensions.k8s.io/workflowtemplates.argoproj.io created 
    serviceaccount/argo created 
    serviceaccount/argo-server created 
    serviceaccount/mast created 
    role.rbac.authorization.k8s.io/argo-role created 
    clusterrole.rbac.authorization.k8s.io/argo-aggregate-to-admin created 
    clusterrole.rbac.authorization.k8s.io/argo-aggregate-to-edit created 
    clusterrole.rbac.authorization.k8s.io/argo-aggregate-to-view created 
    clusterrole.rbac.authorization.k8s.io/argo-cluster-role created 
    clusterrole.rbac.authorization.k8s.io/argo-server-cluster-role created 
    clusterrole.rbac.authorization.k8s.io/mast-server-cr created 
    rolebinding.rbac.authorization.k8s.io/argo-binding created 
    clusterrolebinding.rbac.authorization.k8s.io/argo-binding created 
    clusterrolebinding.rbac.authorization.k8s.io/argo-server-binding created 
    clusterrolebinding.rbac.authorization.k8s.io/mast-server-rolebinding-cluster created 
    configmap/workflow-controller-configmap created service/argo-server created 
    service/workflow-controller-metrics created 
    deployment.apps/argo-server created deployment.apps/mast-server created 
    deployment.apps/workflow-controller created 
    ```

Wait until all pods are up and running.

### Install CLI

Cli for diferent OS types and architectures can be find on [releases page](https://gitlab.com/ansi-services/mast/-/releases), provided with instalation instructions as well.

## Add your firsr MastJob and run it

To submit your firts MastJob you first need to add playbook so mast would know what to do. For this example we will use a simple playbook that will print "Hello World" to the console.

Simply add it by:

```bash
kubectl apply -f https://gitlab.com/ansi-services/mast/-/raw/master/examples/hello-world/hello-world-playbook.yaml
```

Now you can create MastJob object that will use this playbook. Create it with mast CLI:

```bash
mast job create https://gitlab.com/ansi-services/mast/-/raw/master/examples/hello-world/hello-world-mastjob.yaml
```

You can do it also with standard `kubectl apply -f` command.

??? example "output"
    ```bash 
    INFO[0000] MastJob hello-world created 
    ```

MastJob is just a definition of action or set of actions to do, to start MastJob you need a MastRun object.MastRun acts as a trigger. It can be created as a code, defined in .yaml file or dynamicly with Mast CLI.

For now use CLI:

```bash
mast job run hello-world
```

??? example "output"
    ```bash 
    INFO[0000] MastRun created, job hello-world-xxxxx is starting 
    ```

This will generate MastRun and add it to the cluster.

You can list all MastRuns with:

```bash
mast run list
```

??? example "output"
    ```bash 
    NAME                    STATUS    AGE 
    hello-world-xxxxx       Running   14m 
    ```

You can see that MastRun name is `hello-world-` ad a 5 random characters. This is done to diferenciate Mastruns who run the same Mastjob for few times.

Finally to see that promissed "hello world" run:

```bash
mast run logs <MastJob_name>
```

??? example "output"

    ```bash
      PLAY [localhost] ***************************************************************

      TASK [Gathering Facts] *********************************************************
      ok: [localhost]

      TASK [Hello from ansible inside your cluster] **********************************
      ok: [localhost] => {
          "msg": "Hello there"
      }

      PLAY RECAP *********************************************************************
      localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    ```

Just add right MastJob name at the end of command.

