# Operator

With Mast, you can create a simple and lightweight Kubernetes operator.

## Steps

### Create Mastjob

First, you need to create MastJob with your desired action to be performed when certain conditions are met. This is nothing new, you can check how to do it [here](../mastjob/index.md)

### Create MastRun

Then create a Mastrun, you can override some configuration in it if you need.

!!! note 
    It is important to set `template: true`, because you don't want to run immediately.

### Create MastOperator

MastOperator is a third CRD in Mast. This is where you set what Mastjob will be started and on what conditions.

Let`s see how looks the definition of MastOperator.

```yaml
apiVersion: mast.ansi.services/v1
kind: MastOperator
metadata:
    name: my-operator
spec:
    event:
        objects: # (1)
            - pods
        namespaces: # (2)
            - all
        actions: # (3)
            - create
            - update
    run:
        mastRunRef: # (4)
          onEvent: javaapp-operator-mr
```

1.  `objects` is where you set which object MastOperator should observe. It can be any object type available in Kubernetes cluster, including CRDs.
2.  Here, you set in which namespaces MastOperator should observe objects set earlier. You can pass desired namespace's names or put `all` for all namespaces in cluster.
3.  Set actions which performed on object types in set namespaces will trigger MastJob. For now, MastOperator supports `create`, `update` and `delete`.
4.  `mastRunRef` can have one of two forms. It can contain one field `onEvent` which mean for every event we are subscribed run same MastRun. If `onEvent` is defined rest of options is ignored. Second option gives oportunity to serve different MastRun's for different actions, you can do it by setting `onCreate`, `onUpdate` or `onDelete` fields. In second option, if there is registered action without handler it will raise error.


or it can look like this for custom handlers:


```yaml
apiVersion: mast.ansi.services/v1
kind: MastOperator
metadata:
    name: my-operator
spec:
    event:
        objects: # (1)
            - pods
        namespaces: # (2)
            - all
        actions: # (3)
            - create
            - update
    run:
        mastRunRef: # (4)
          onCreate: javaapp-operator-create
          onUpdate: javaapp-operator-update
```


Check code annotations to know how to set up a MastOperator.

### passing variables from triggering object to MastJob

More advanced Operators may require to pass some variables from object which triggers MastOperator to MastJob executed on MastOperator`s behalf.

Imagine a case where you deploy a new Java App. You want to automatically create a service for it, but you would like to name it the same name as Java App pod`s for better discovery in the future.

This is similar to rendering variables in Ansible using Jinja2 templating. You have to set variables in vars section either in MastJob or MastRun. See example below.

You want to pass the name of the object which triggered the operator. In MastJob, you will create that var like this:

```yaml
apiVersion: mast.ansi.services/v1
kind: MastJob
metadata:
  name: test-vars-literal
  namespace: mast
spec:
    ...
    vars:
      - fromLiteral:
          - name_of_triggerring_object: "{resource.metadata.name}"
    ...
```

!!! note 
    The Word `resource` is a key under which the specification of the triggering object is stored. To access a certain value just go down the schema, like above to get the object name which is always in metadata.
