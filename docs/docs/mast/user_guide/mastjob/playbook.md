# Playbook

Playbook is the most important part of MastJob`s configuration. 

It is **required** to set at least one playbook. 
But you can define as many as you want. Playbooks will be executed in order you set them in MastJob

There are to ways to set playbook which will be run.

## Playbook from ConfigMap

You can submit the playbook as ConfigMap and then mention it name and key under which is your playbook.

```yaml title="playbook-configmap.yaml"
apiVersion: v1
kind: ConfigMap
metadata:
  name: playbook-config
  namespace: mast
data:
  playbook.yaml: |
    ---
    - hosts: all,localhost
      tasks:
        - name: test connection
          ping:

        - name: hello
          debug:
            msg: "hello"
```

```yaml title="mastjob"
spec:
  ...
  configuration:
    playbook:
      - fromConfigMap:
          name: playbook-config
          key: playbook.yaml
```

## from repository

`fromRepo` means that you can point to playbook from your repository mentioned in the `repository` section. Path mast be path in your repo where your desired file is located

```yaml
spec:
   ...
   configuration:
    playbook:
      - fromRepo:
          path: ansible-hello/playbook1.yaml

```