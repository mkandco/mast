# MastRun

MastRun is a **"call to action"** resource. By creating it, either by submitting the file to The cluster or by generating it with CLI, only the presence of MastRun will trigger Mast Server to start actions defined in MastJob. So, the simplest MastRun looks like this:

```yaml
--8<-- "./../controller/tests/test_manifests/mr-git-basic-auth.yaml"
```

<!-- ```yaml
apiVersion: mast.ansi.services/v1
kind: MastRun
metadata:
    name: my-mastrun
    namespace: mast
spec:
    mastJobRef:
        name: my-mastjob
``` -->

## Overriding

MastRun has one more key functionality. In MastRun, you can override the configuration set in MastJob. This is a bit similar to inheritance in programming. For example, you can override a `vars` section by adding one more file with variables because you want to set some service a little different due to other environment.

MastRun has a section `specOverride` which has the same structure as MastJob's `spec`.

!!! note warning
    While all parameters are overridden during the execute of MastJob, `vars` are the only one being extended. This is by design setup due to vars override in Ansible itself.

```yaml
--8<-- "./../controller/tests/test_manifests/mr-vars-override.yaml"
```

<!-- ```yaml
apiVersion: mast.ansi.services/v1
kind: MastRun
metadata:
  name: mr-vars-override
spec:
  mastJobRef:
    name: mj-vars-override
    specOverride:
      ansible:
        version: latest
      workflowName: new-workflow
      configuration:
        playbook:
          - fromConfigMap:
              name: playbook-config2
              key: playbook.yaml
        vars:
          - fromConfigMap:
              name: vars-config2
              key: vars.yaml
          - fromExtRepo:
              git:
                url: https://github.com/xyz.git
                path: vars.yaml
          - fromLiteral:
              - my_extra_repo_var: 'i am literal override'
``` -->

## Temaplate

You can set MastRun to be template. When template parameter is te to `true` MastRun won't trigger the execution of MastJob. This can be also usefull when you very often override the same MastJob and you want to store this MastRun in the cluster.

```yaml
apiVersion: mast.ansi.services/v1
kind: MastRun
metadata:
  name: mastrun-template
spec:
  template: true
  mastJobRef:
    name: masjtob-name
```

This is also used by [MastOperator](../operator/index.md). It uses the template of MastRun to generate MastRuns if new event occurs.
