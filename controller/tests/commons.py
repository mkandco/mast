import subprocess
import time
import kopf.testing
import pytest
from pathlib import Path
from typing import List


ROOT_DIR = Path(__file__).resolve().parent.parent.parent

TEST_MANIFESTS_DIR = Path(__file__).resolve().parent.joinpath("test_manifests")

CLI_DIR = Path(__file__).resolve().parent.parent.parent.joinpath("cli")

MAST_SERVER = ROOT_DIR.joinpath("controller/src/main.py")

EXAMPLES_MANIFESTS_DIR = ROOT_DIR.joinpath("examples")

# To prevent lengthy threads in the loop executor when the process exits.
settings = kopf.OperatorSettings()
settings.watching.server_timeout = 10


@pytest.fixture()
def cleanup_mastjobs():
    yield
    subprocess.run(["kubectl", "delete", "mj", "--all"], shell=False)
    time.sleep(3)


@pytest.fixture()
def cleanup_mastruns():
    yield
    subprocess.run(["kubectl", "delete", "mr", "--all"], shell=False)
    time.sleep(3)


@pytest.fixture()
def cleanup_mastoperators():
    yield
    subprocess.run(["kubectl", "delete", "mo", "--all"], shell=False)
    time.sleep(3)


def wait_for_workflow_to_finish(workflow_name: str) -> None:
    while True:
        time.sleep(15)
        workflow_status = subprocess.run(
            ["kubectl", "get", "wf", workflow_name, "--template={{.metadata.labels}}"],
            shell=False,
            check=True,
            capture_output=True,
        )
        print("Waiting for workflow %s to finish" % workflow_name)

        if "workflows.argoproj.io/completed:true" in workflow_status.stdout.decode(
            "utf-8"
        ):
            break


def wait_for_argo_to_start() -> None:
    while True:
        time.sleep(10)
        argo_server = subprocess.run(
            [
                "kubectl",
                "get",
                "pod",
                "--selector=app=argo-server",
                "-o",
                "jsonpath='{.items[0].status.phase}'",
            ],
            shell=False,
            check=True,
            capture_output=True,
            text=True,
        )
        workflow_controller = subprocess.run(
            [
                "kubectl",
                "get",
                "pod",
                "--selector=app=workflow-controller",
                "-o",
                "jsonpath='{.items[0].status.phase}'",
            ],
            shell=False,
            check=True,
            capture_output=True,
            text=True,
        )
        print("Waiting for argo's pods to start")
        if (
            argo_server.stdout == "'Running'"
            and workflow_controller.stdout == "'Running'"
        ):
            break


def save_workflow_logs(workflow_name: str) -> None:
    pods: List[str] = subprocess.run(
        [
            "kubectl",
            "get",
            "pod",
            f"--selector=workflows.argoproj.io/workflow={workflow_name}",
            "--output=jsonpath={.items..metadata.name}",
        ],
        shell=False,
        check=True,
        capture_output=True,
    )

    for pod in pods:
        logs = subprocess.run(
            ["kubectl", "logs", workflow_name, "main"],
            shell=False,
            check=True,
            capture_output=True,
        )
        # save logs to file
