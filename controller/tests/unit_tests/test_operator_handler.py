import pytest
import copy
from src.operator_handler import OperatorHandler, action_to_handler

get_operator_list = lambda handler, obj, action, namespace: handler._handled_resources[obj][action][namespace]
get_operator_meta = lambda handler, obj, action, namespace: get_operator_list(handler, obj, action, namespace)[0]
operator_spec_to_tuple = lambda spec: (spec["obj"], spec["spec"], spec["name"])

def assert_operator_obj(operator_meta, name, ref):
    assert operator_meta.name == name
    assert operator_meta.mastrun == ref

@pytest.fixture
def empty_operator_handler():
    return OperatorHandler()

@pytest.fixture(scope="session")
def create_action():
    return "create"

@pytest.fixture(scope="session")
def update_action():
    return "update"

@pytest.fixture(scope="session")
def default_namespace():
    return "default"

@pytest.fixture(scope="session")
def main_namespace():
    return "main"

@pytest.fixture(scope="session")
def all_namespaces():
    return "all"

@pytest.fixture(scope="session")
def simple_mastrun_ref():
    return "test_mast_run"

@pytest.fixture(scope="session")
def simple_mastrun_ref_updated():
    return "test_mast_run_2"

@pytest.fixture(scope="class")
def simple_operator_spec(create_action, default_namespace, simple_mastrun_ref):
    return {"obj": "deployments", 
            "name": "modify_deploy", 
            "spec": {
                "run": {"mastRunRef": {
                    "onEvent": simple_mastrun_ref
                    }
                },
                "event": {
                    "actions": [create_action],
                    "namespaces": [default_namespace]
                }
            }
        }

@pytest.fixture
def simple_operator_spec_updated(simple_operator_spec, create_action, update_action):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["spec"]["event"]["actions"] = [create_action, update_action]
    return operator_spec

@pytest.fixture
def simple_operator_main_ns(simple_operator_spec, main_namespace):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["spec"]["event"]["namespaces"] = [main_namespace]
    return operator_spec

@pytest.fixture
def simple_operator_two_ns(simple_operator_spec, main_namespace, default_namespace):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["spec"]["event"]["namespaces"] = [main_namespace, default_namespace]
    return operator_spec

@pytest.fixture
def simple_operator_changed_mr(simple_operator_spec, simple_mastrun_ref_updated):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["spec"]["run"]["mastRunRef"]["onEvent"] = simple_mastrun_ref_updated
    return operator_spec

@pytest.fixture
def handler_with_operator(empty_operator_handler, simple_operator_spec):
    operator_handler = empty_operator_handler
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec)
    operator_handler.append(obj, spec, name)
    return operator_handler

@pytest.fixture
def all_namespaces_operator_spec(simple_operator_spec, all_namespaces):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["name"] = "modify_deploy_all"
    operator_spec["spec"]["event"]["namespaces"] = [all_namespaces]
    return operator_spec

@pytest.fixture
def all_namespaces_operator_spec_updated(all_namespaces_operator_spec, create_action, update_action):
    operator_spec = copy.deepcopy(all_namespaces_operator_spec)
    operator_spec["spec"]["event"]["actions"] = [create_action, update_action]
    return operator_spec

@pytest.fixture
def handler_with_all_ns_operator(empty_operator_handler, all_namespaces_operator_spec):
    operator_handler = empty_operator_handler
    obj, spec, name = operator_spec_to_tuple(all_namespaces_operator_spec)
    operator_handler.append(obj, spec, name)
    return operator_handler

@pytest.fixture
def handler_with_two_operators(handler_with_all_ns_operator, simple_operator_spec):
    operator_handler = handler_with_all_ns_operator
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec)
    operator_handler.append(obj, spec, name)
    return operator_handler

@pytest.fixture
def handler_with_two_refs(handler_with_all_ns_operator, simple_operator_changed_mr):
    operator_handler = handler_with_all_ns_operator
    obj, spec, name = operator_spec_to_tuple(simple_operator_changed_mr)
    operator_handler.append(obj, spec, name)
    return operator_handler

@pytest.fixture
def simple_operator_spec_with_multiple_actions(simple_operator_spec, update_action):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["name"] = "modify_deploy_on_cu"
    operator_spec["spec"]["event"]["actions"].append(update_action)
    return operator_spec

@pytest.fixture
def action_to_handler_mapping(simple_mastrun_ref, simple_mastrun_ref_updated):
    return {"onCreate": simple_mastrun_ref, "onUpdate": simple_mastrun_ref_updated}

@pytest.fixture
def simple_operator_diferent_actions(simple_operator_spec_with_multiple_actions, 
                                     action_to_handler_mapping):
    operator_spec = copy.deepcopy(simple_operator_spec_with_multiple_actions)
    del operator_spec["spec"]["run"]["mastRunRef"]["onEvent"]
    for action_ref, mastrun_ref in action_to_handler_mapping.items():
        operator_spec["spec"]["run"]["mastRunRef"][action_ref] = mastrun_ref
    return operator_spec

@pytest.fixture
def handler_with_different_actions_operator(empty_operator_handler, simple_operator_diferent_actions):
    operator_handler = empty_operator_handler
    obj, spec, name = operator_spec_to_tuple(simple_operator_diferent_actions)
    operator_handler.append(obj, spec, name)
    return operator_handler

@pytest.fixture
def simple_operator_with_not_matching_actions(simple_operator_spec_with_multiple_actions,
                                              simple_mastrun_ref):
    operator_spec = copy.deepcopy(simple_operator_spec_with_multiple_actions)
    del operator_spec["spec"]["run"]["mastRunRef"]["onEvent"]
    operator_spec["spec"]["run"]["mastRunRef"]["onCreate"] = simple_mastrun_ref
    return operator_spec

@pytest.fixture
def simple_operator_spec_for_pods(simple_operator_spec):
    operator_spec = copy.deepcopy(simple_operator_spec)
    operator_spec["name"] = "modify_pod_on_create"
    operator_spec["obj"] = "pods"
    return operator_spec

@pytest.fixture
def simple_operator_spec_mutli_actions_updated(simple_operator_spec_with_multiple_actions, create_action):
    operator_spec = copy.deepcopy(simple_operator_spec_with_multiple_actions)
    operator_spec["spec"]["event"]["actions"] = [create_action]
    return operator_spec

@pytest.fixture
def handler_with_multi_actions_operator(empty_operator_handler, simple_operator_spec_with_multiple_actions):
    operator_handler = empty_operator_handler
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec_with_multiple_actions)
    operator_handler.append(obj, spec, name)
    return operator_handler


def test_00_append_operator(empty_operator_handler, simple_operator_spec, 
                            create_action, default_namespace, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec)
    empty_operator_handler.append(obj, spec, name)
    operator_meta = get_operator_meta(empty_operator_handler, obj, create_action, default_namespace)
    assert operator_meta.name == name
    assert operator_meta.mastrun == simple_mastrun_ref

def test_01_append_operator_with_all_namespaces(empty_operator_handler, all_namespaces_operator_spec,
                                                create_action, all_namespaces, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(all_namespaces_operator_spec)
    empty_operator_handler.append(obj, spec, name)
    operator_meta = get_operator_meta(empty_operator_handler, obj, create_action, all_namespaces)
    assert_operator_obj(operator_meta, name, simple_mastrun_ref)

def test_02_append_operator_with_multiple_actions(empty_operator_handler, simple_operator_spec_with_multiple_actions,
                                                create_action, update_action, default_namespace, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec_with_multiple_actions)
    empty_operator_handler.append(obj, spec, name)
    for action in (create_action, update_action):
        operator_meta = get_operator_meta(empty_operator_handler, obj, action, default_namespace)
        assert_operator_obj(operator_meta, name, simple_mastrun_ref)

def test_03_delete_operator(handler_with_operator, simple_operator_spec, create_action, default_namespace):
    obj, _, name = operator_spec_to_tuple(simple_operator_spec)
    handler_with_operator.delete(obj, name)
    assert len(get_operator_list(handler_with_operator, obj, create_action, default_namespace)) == 0

def test_04_delete_operator_with_all_namespaces(handler_with_all_ns_operator, all_namespaces_operator_spec, 
                                                create_action, all_namespaces):
    obj, _, name = operator_spec_to_tuple(all_namespaces_operator_spec)
    handler_with_all_ns_operator.delete(obj, name)
    assert len(get_operator_list(handler_with_all_ns_operator, obj, create_action, all_namespaces)) == 0

def test_05_delete_operator_with_multiple_actions(handler_with_multi_actions_operator, 
                                                  simple_operator_spec_with_multiple_actions,
                                                  create_action, update_action, default_namespace):
    obj, _, name = operator_spec_to_tuple(simple_operator_spec_with_multiple_actions)
    handler_with_multi_actions_operator.delete(obj, name)
    for action in (create_action, update_action):
        assert len(get_operator_list(handler_with_multi_actions_operator, obj, action, default_namespace)) == 0

def test_06_delete_operator_that_not_exists(empty_operator_handler, simple_operator_spec):
    obj, _, name = operator_spec_to_tuple(simple_operator_spec)
    with pytest.raises(LookupError):
        empty_operator_handler.delete(obj, name)

def test_06B_delete_operator_that_not_exists_when_others(handler_with_all_ns_operator, simple_operator_spec):
    obj, _, name = operator_spec_to_tuple(simple_operator_spec)
    with pytest.raises(LookupError):
        handler_with_all_ns_operator.delete(obj, name)

def test_07_update_operator(handler_with_operator, simple_operator_spec_updated, 
                            create_action, update_action, default_namespace, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec_updated)
    handler_with_operator.update(obj, spec, name)
    for action in (create_action, update_action):
        operator_meta = get_operator_meta(handler_with_operator, obj, action, default_namespace)
        assert_operator_obj(operator_meta, name, simple_mastrun_ref)

def test_08_update_operator_with_all_namespaces(handler_with_all_ns_operator, all_namespaces_operator_spec_updated,
                                                create_action, update_action, all_namespaces, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(all_namespaces_operator_spec_updated)
    handler_with_all_ns_operator.update(obj, spec, name)
    for action in (create_action, update_action):
        operator_meta = get_operator_meta(handler_with_all_ns_operator, obj, action, all_namespaces)
        assert_operator_obj(operator_meta, name, simple_mastrun_ref)

def test_09_update_operator_with_multiple_actions(handler_with_multi_actions_operator, 
                                                  simple_operator_spec_mutli_actions_updated,
                                                  create_action, update_action, default_namespace, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(simple_operator_spec_mutli_actions_updated)
    handler_with_multi_actions_operator.update(obj, spec, name)
    operator_meta = get_operator_meta(handler_with_multi_actions_operator, obj, create_action, default_namespace)
    assert_operator_obj(operator_meta, name, simple_mastrun_ref)
    assert len(get_operator_list(handler_with_multi_actions_operator, obj, update_action, default_namespace)) == 0
    
def test_10_update_operator_that_not_exists(handler_with_operator, all_namespaces_operator_spec_updated):
    obj, spec, name = operator_spec_to_tuple(all_namespaces_operator_spec_updated)
    with pytest.raises(LookupError):
        handler_with_operator.update(obj, spec, name)

def test_11_update_operator_change_namespaces(handler_with_operator, simple_operator_main_ns, default_namespace,
                                              main_namespace, create_action, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(simple_operator_main_ns)
    handler_with_operator.update(obj, spec, name)
    operator_meta = get_operator_meta(handler_with_operator, obj, create_action, main_namespace)
    assert_operator_obj(operator_meta, name, simple_mastrun_ref)
    assert len(get_operator_list(handler_with_operator, obj, create_action, default_namespace)) == 0

def test_12_update_operator_add_namespace(handler_with_operator, simple_operator_two_ns, default_namespace,
                                              main_namespace, create_action, simple_mastrun_ref):
    obj, spec, name = operator_spec_to_tuple(simple_operator_two_ns)
    handler_with_operator.update(obj, spec, name)
    for ns in (default_namespace, main_namespace):
        operator_meta = get_operator_meta(handler_with_operator, obj, create_action, ns)
        assert_operator_obj(operator_meta, name, simple_mastrun_ref)

def test_13_update_operator_change_mastrun(handler_with_operator, simple_operator_changed_mr, default_namespace,
                                           create_action, simple_mastrun_ref_updated):
    obj, spec, name = operator_spec_to_tuple(simple_operator_changed_mr)
    handler_with_operator.update(obj, spec, name)
    operator_meta = get_operator_meta(handler_with_operator, obj, create_action, default_namespace)
    assert_operator_obj(operator_meta, name, simple_mastrun_ref_updated)
    
def test_14_find_operator(handler_with_operator, simple_operator_spec, default_namespace, create_action):
    obj = simple_operator_spec["obj"]
    result = handler_with_operator.find(obj, default_namespace, create_action)
    operator_meta = get_operator_meta(handler_with_operator, obj, create_action, default_namespace)
    for operator in result:
        assert operator_meta == operator

def test_15_find_operator_with_all_namespaces(handler_with_all_ns_operator, all_namespaces_operator_spec,
                                              default_namespace, all_namespaces, create_action):
    obj = all_namespaces_operator_spec["obj"]
    result = handler_with_all_ns_operator.find(obj, default_namespace, create_action)
    operator_meta = get_operator_meta(handler_with_all_ns_operator, obj, create_action, all_namespaces)
    for operator in result:
        assert operator_meta == operator

def test_16_find_operator_multiple_handlers_same_ref(handler_with_two_operators, all_namespaces_operator_spec, 
                                                     all_namespaces, default_namespace, create_action):
    obj = all_namespaces_operator_spec["obj"]
    result = handler_with_two_operators.find(obj, default_namespace, create_action)
    operator_meta = get_operator_meta(handler_with_two_operators, obj, create_action, all_namespaces)
    for operator in result:
        assert operator_meta == operator

def test_17_find_operator_multiple_handlers(handler_with_two_refs, all_namespaces_operator_spec, 
                                             all_namespaces, default_namespace, create_action):
    obj = all_namespaces_operator_spec["obj"]
    result = handler_with_two_refs.find(obj, default_namespace, create_action)
    operator_meta_all = get_operator_meta(handler_with_two_refs, obj, create_action, all_namespaces)
    operator_meta_simple = get_operator_meta(handler_with_two_refs, obj, create_action, default_namespace)
    operators = [operator_meta_all, operator_meta_simple]
    for operator in result:
        if operator in operators:
            operators.remove(operator)
    assert len(operators) == 0

def test_18_find_operator_that_not_exists(empty_operator_handler, simple_operator_spec,
                                          default_namespace, create_action):
    obj = simple_operator_spec["obj"]
    result = empty_operator_handler.find(obj, default_namespace, create_action)
    assert len(list(result)) == 0

def test_19_find_operator_with_not_registered(handler_with_operator, simple_operator_spec,
                                              default_namespace, update_action):
    obj = simple_operator_spec["obj"]
    result = handler_with_operator.find(obj, default_namespace, update_action)
    assert len(list(result)) == 0

def test_20_find_operator_with_not_registered_resource(handler_with_operator, simple_operator_spec_for_pods,
                                                       default_namespace, create_action):
    obj = simple_operator_spec_for_pods["obj"]
    result = handler_with_operator.find(obj, default_namespace, create_action)
    assert len(list(result)) == 0

def test_21_check_if_operator_is_in_scope(handler_with_operator, simple_operator_spec, 
                                          default_namespace, create_action):
    obj = simple_operator_spec["obj"]
    result = handler_with_operator.has_operators_for_scope(obj, default_namespace, create_action)
    assert result

def test_22_append_operator_with_different_refs_for_actions(empty_operator_handler, create_action, update_action,
                                                            simple_operator_diferent_actions, default_namespace,
                                                            action_to_handler_mapping):
    obj, spec, name = operator_spec_to_tuple(simple_operator_diferent_actions)
    empty_operator_handler.append(obj, spec, name)
    for action in (create_action, update_action):
        operator_meta = get_operator_meta(empty_operator_handler, obj, action, default_namespace)
        mastrun_ref = action_to_handler_mapping[action_to_handler(action)]
        assert_operator_obj(operator_meta, name, mastrun_ref)

def test_23_append_operator_with_different_refs_not_matching_actions(simple_operator_with_not_matching_actions,
                                                                     empty_operator_handler):
    obj, spec, name = operator_spec_to_tuple(simple_operator_with_not_matching_actions)
    with pytest.raises(LookupError):
        empty_operator_handler.append(obj, spec, name)

def test_24_find_operator_when_different_actions(handler_with_different_actions_operator,
                                                 simple_operator_diferent_actions, create_action, update_action,
                                                 default_namespace, simple_mastrun_ref, simple_mastrun_ref_updated,
                                                 action_to_handler_mapping):
    obj = simple_operator_diferent_actions["obj"]
    for action in (create_action, update_action):
        result = handler_with_different_actions_operator.find(obj, default_namespace, action)
        operator_meta = get_operator_meta(handler_with_different_actions_operator, 
                                            obj, action, default_namespace)

        operator = next(result)
        assert operator == operator_meta
        assert operator.mastrun == action_to_handler_mapping[action_to_handler(action)]