from src.utils import render_variables_from_resource


def test_render_variables_from_resource():
    """test render variables from resource"""
    resource = {"spec": {"name": "test_name", "var": "test_var"}}
    mastjob = {
        "spec": {
            "vars": [
                {"name": "{{resource.spec.name}}", "value": "{{resource.spec.var}}"}
            ]
        }
    }
    assert render_variables_from_resource(mastjob, resource) == {
        "spec": {"vars": [{"name": "test_name", "value": "test_var"}]}
    }


def test_render_variables_from_resource_without_variables_specified():
    """test render variables from resource but not specify any variable from resource in mastjob"""
    resource = {"spec": {"name": "test_name", "var": "test_var"}}
    mastjob = {"spec": {"vars": [{"name": "default_name", "value": "deafult_var"}]}}
    assert render_variables_from_resource(mastjob, resource) == {
        "spec": {"vars": [{"name": "default_name", "value": "deafult_var"}]}
    }
