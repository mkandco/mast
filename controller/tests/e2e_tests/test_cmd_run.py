import subprocess
import time
import kopf.testing
from tests.commons import (
    TEST_MANIFESTS_DIR,
    CLI_DIR,
    MAST_SERVER,
    cleanup_mastjobs,
    cleanup_mastruns,
    settings,
    cleanup_mastoperators,
    wait_for_workflow_to_finish,
)

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-git-basic-auth.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-git-basic-auth.yaml")

test_secret_basic_auth_obj_yaml = TEST_MANIFESTS_DIR.joinpath(
    "test-secret-git-basic-auth.yaml"
)


class TestCmdRun:
    """
    test for the cmd mast run
    """

    def test_mast_run_list_with_empty_return(self):
        """mast run list - check  no runs returned"""

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "run", "list"],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )

        assert "No MustRuns found" in mast_cmd.stderr.decode("utf-8")

    def test_mast_run_start_from_file(self, cleanup_mastjobs, cleanup_mastruns):
        """mast run start - check if mastrun is created and workflow triggered"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "apply", "-f", mj_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)
            mast_cmd = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "start", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

            get_mr = subprocess.run(
                ["kubectl", "get", "mr"],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

            get_wf = subprocess.run(
                ["kubectl", "get", "wf", "-l", "mastrun=git-basic-auth"],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

        assert runner.exception is None
        assert runner.exit_code == 0
        assert "MastRun git-basic-auth created" in mast_cmd.stderr.decode("utf-8")
        assert "git-basic-auth" in get_mr.stdout.decode("utf-8")

    def test_mast_run_start_from_template(self, cleanup_mastjobs, cleanup_mastruns):
        """mast run start - check if mastrun is created and workflow triggered"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "apply", "-f", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)
            mast_cmd = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "start", "git-basic-auth"],
                shell=False,
                timeout=10,
                capture_output=True,
            )
            time.sleep(3)
            get_mr = subprocess.run(
                ["kubectl", "get", "mr"],
                check=True,
                shell=False,
                timeout=10,
                capture_output=True,
            )

        assert runner.exception is None
        assert runner.exit_code == 0
        assert "MastRun", "created" in mast_cmd.stderr.decode("utf-8")
        assert "git-basic-auth" in get_mr.stdout.decode("utf-8")

    def test_mast_run_list_with_one_return(self, cleanup_mastjobs, cleanup_mastruns):
        """mast run list - check one run returned"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "apply", "-f", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(3)

            mast_cmd = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "list"],
                shell=False,
                timeout=10,
                capture_output=True,
            )
        assert runner.exception is None
        assert runner.exit_code == 0
        assert "git-basic-auth" in mast_cmd.stdout.decode("utf-8")

    def test_mast_run_info(self, cleanup_mastjobs, cleanup_mastruns):
        """mast run info - check if info is returned"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "apply", "-f", mj_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)

            subprocess.run(
                ["kubectl", "apply", "-f", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)

            mast_cmd = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "info", "git-basic-auth"],
                shell=False,
                timeout=10,
                capture_output=True,
            )

        assert runner.exception is None
        assert runner.exit_code == 0
        assert "git-basic-auth" in mast_cmd.stdout.decode("utf-8")

    def test_mast_run_abort_with_no_workflow_started(
        self, cleanup_mastjobs, cleanup_mastruns
    ):
        """mast run abort - abort mastrun which workflow did not start yet"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "delete", "-f", test_secret_basic_auth_obj_yaml],
                shell=False,
                timeout=10,
            )
            time.sleep(1)
            subprocess.run(
                ["kubectl", "apply", "-f", mj_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)

            subprocess.run(
                ["kubectl", "apply", "-f", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)

            mast_run_abort = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "abort", "git-basic-auth"],
                shell=False,
                timeout=10,
                capture_output=True,
            )

        assert runner.exception is None
        assert runner.exit_code == 0
        assert "MastRun did not start" in mast_run_abort.stderr.decode("utf-8")

    def test_mast_run_abort_with_workflow_started(
        self, cleanup_mastjobs, cleanup_mastruns
    ):
        """mast run abort - run abort on mastrun with workflow started"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "apply", "-f", test_secret_basic_auth_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(60)

            subprocess.run(
                ["kubectl", "apply", "-f", mj_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)

            subprocess.run(
                ["kubectl", "apply", "-f", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(10)

            mast_run_abort = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "abort", "git-basic-auth"],
                shell=False,
                timeout=10,
                capture_output=True,
            )

        assert runner.exception is None
        assert runner.exit_code == 0
        assert "MastRun git-basic-auth aborted" in mast_run_abort.stderr.decode("utf-8")

    def test_mast_run_logs(self, cleanup_mastjobs, cleanup_mastruns):
        """mast run logs - check if logs are returned"""

        with kopf.testing.KopfRunner(
            ["run", "--all-namespaces", str(MAST_SERVER)],
            timeout=60,
            settings=settings,
        ) as runner:

            subprocess.run(
                ["kubectl", "apply", "-f", test_secret_basic_auth_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(60)

            subprocess.run(
                ["kubectl", "apply", "-f", mj_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(1)

            subprocess.run(
                ["kubectl", "apply", "-f", mr_obj_yaml],
                check=True,
                shell=False,
                timeout=10,
            )
            time.sleep(40)

            workflow_name = subprocess.run(
                [
                    "kubectl",
                    "get",
                    "mastrun",
                    "git-basic-auth",
                    "--template={{.status.mastrun_handler.workFlow}}",
                ],
                shell=False,
                check=True,
                timeout=10,
                capture_output=True,
            )

            wait_for_workflow_to_finish(workflow_name.stdout.decode("utf-8"))

            mast_cmd = subprocess.run(
                [CLI_DIR.joinpath("mast"), "run", "logs", "git-basic-auth"],
                shell=False,
                timeout=10,
                capture_output=True,
            )
        assert runner.exception is None
        assert runner.exit_code == 0
        assert "PLAY" in mast_cmd.stderr.decode("utf-8")
