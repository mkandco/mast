import subprocess
import time
import kopf.testing
import pytest
from tests.commons import (
    EXAMPLES_MANIFESTS_DIR,
    MAST_SERVER,
    CLI_DIR,
    wait_for_workflow_to_finish,
)

"""
Test to check if the hello-world example works as expected.
"""

mj_obj_yaml = EXAMPLES_MANIFESTS_DIR.joinpath("hello-world/hello-world-mastjob.yaml")

playbook = EXAMPLES_MANIFESTS_DIR.joinpath("hello-world/hello-world-playbook.yaml")


def test_example_hello_world():
    # To prevent lengthy threads in the loop executor when the process exits.
    settings = kopf.OperatorSettings()
    settings.watching.server_timeout = 10
    # Run an operator and simulate some activity with the operated resource.
    with kopf.testing.KopfRunner(
        ["run", "--all-namespaces", str(MAST_SERVER)],
        timeout=60,
        settings=settings,
    ) as runner:
        subprocess.run(
            ["kubectl", "apply", "-f", mj_obj_yaml], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        subprocess.run(
            ["kubectl", "apply", "-f", playbook], shell=False, check=True, timeout=10
        )
        time.sleep(1)  # give it some time to react
        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "job", "run", "hello-world"],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(5)

        workflow_name = subprocess.run(
            [
                "kubectl",
                "get",
                "wf",
                "-l",
                "mastjob=hello-world",
                "-o",
                "custom-columns=:metadata.name",
            ],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
            text=True,
        )
        workflow_name = workflow_name.stdout.strip("\n")
        wait_for_workflow_to_finish(workflow_name)

        workflow_status = subprocess.run(
            [
                "kubectl",
                "get",
                "wf",
                workflow_name,
                "--template={{.metadata.labels}}",
            ],
            shell=False,
            check=True,
            timeout=10,
            capture_output=True,
        )

    assert "Succeeded" in workflow_status.stdout.decode("utf-8")
    assert "MastRun created, job hello-world" in mast_cmd.stderr.decode("utf-8")
    assert runner.exception is None
    assert runner.exit_code == 0
    # assert "[mast/hello-world] Handler 'mastrun_handler' succeeded." in runner.stdout
    # assert (
    #     "[mast/hello-world] Creation is processed: 1 succeeded; 0 failed."
    #     in runner.stdout
    # )
