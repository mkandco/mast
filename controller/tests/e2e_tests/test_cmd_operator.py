import subprocess
import time
from tests.commons import (
    TEST_MANIFESTS_DIR,
    CLI_DIR,
    cleanup_mastjobs,
    cleanup_mastruns,
    settings,
    cleanup_mastoperators,
)

mj_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mj-git-basic-auth.yaml")


mo_obj_yaml = TEST_MANIFESTS_DIR.joinpath("project-operator.yaml")

mr_obj_yaml = TEST_MANIFESTS_DIR.joinpath("mr-git-basic-auth.yaml")

test_secret_basic_auth_obj_yaml = TEST_MANIFESTS_DIR.joinpath(
    "test-secret-git-basic-auth.yaml"
)


class TestCmdOperator:
    """
    test for the cmd mast operator
    """

    def test_mast_operator_list_with_empty_return(self):
        """mast operator list - check  no operators returned"""

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "operator", "list"],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)

        assert "No must operators found" in mast_cmd.stderr.decode("utf-8")

    def test_mast_operator_register(self, cleanup_mastoperators):
        """mast operator register - check if mast operator is created"""

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "operator", "register", mo_obj_yaml],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )

        assert "MastOperator project-operator registered" in mast_cmd.stderr.decode(
            "utf-8"
        )

    def test_mast_operator_list_with_one_operator(self, cleanup_mastoperators):
        """mast operator list - check one operator returned"""

        subprocess.run(
            ["kubectl", "apply", "-f", mo_obj_yaml], check=True, shell=False, timeout=10
        )

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "operator", "list"],
            check=True,
            shell=False,
            timeout=10,
            capture_output=True,
        )

        assert "project-operator" in mast_cmd.stdout.decode("utf-8")

    def test_mast_operator_deregister_by_name(self, cleanup_mastoperators):
        """mast operator deregister - check if mast operator is deregistered"""

        subprocess.run(
            ["kubectl", "apply", "-f", mo_obj_yaml], check=True, shell=False, timeout=10
        )

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "operator", "deregister", "project-operator"],
            shell=False,
            timeout=10,
            capture_output=True,
        )
        time.sleep(1)
        assert "MastOperator project-operator deregistered" in mast_cmd.stderr.decode(
            "utf-8"
        )

    def test_mast_operator_deregister_by_file(self, cleanup_mastoperators):
        """mast operator deregister - check if mast operator is deregistered"""

        subprocess.run(
            ["kubectl", "apply", "-f", mo_obj_yaml], check=True, shell=False, timeout=10
        )

        mast_cmd = subprocess.run(
            [CLI_DIR.joinpath("mast"), "operator", "deregister", mo_obj_yaml],
            shell=False,
            timeout=10,
            capture_output=True,
        )

        assert "MastOperator", "deregistered" in mast_cmd.stderr.decode("utf-8")
